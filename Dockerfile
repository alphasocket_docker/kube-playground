FROM ubuntu:16.04
RUN apt update \
    && apt install curl apt-transport-https vim docker.io \
    && echo "deb https://apt.kubernetes.io kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list \
    && curl -sSL https://apt.kubernetes.io/doc/apt-key.gpg | apt-key add --
